export interface ISocial {
  name: string;
  url: string;
  icon: string;
}

export const socials: ISocial[] = [
  {
    name: 'LinkedIn',
    url: 'https://www.linkedin.com/in/arvin-ansari/',
    icon: 'linkedin.svg',
  },
  {
    name: 'GitHub',
    url: 'https://github.com/DeveloperTheExplorer',
    icon: 'github.svg',
  },
  {
    name: 'Twitter',
    url: 'https://twitter.com/arvin_ansari',
    icon: 'x.svg',
  },
];
