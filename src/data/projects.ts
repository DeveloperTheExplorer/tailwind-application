import { IProject } from '../components/project';

export const projects: IProject[] = [
  {
    title: 'Jasmine Energy',
    role: 'Senior Full-Stack Engineer',
    description: 'Jasmine is a decentralized market for climate assets.',
    achievements: [
      'Joined the team after seed funding, <b>re-developed the backend and frontend from scratch</b>, going live in 3 months.',
      'Created a monorepo that supports <b>easy deployments of react apps and nestjs services</b> to AWS. Currently hosting 3 React apps and 3 backend services.',
      'Optimized GitHub Actions CI/CD pipeline to speed up build times by <b>more than 48x</b>',
      '<b>Developed an internal bot</b> within 2 weeks that together with ChatGP Assitant API enabled admin to perform any needed admin tasks from Slack.',
    ],
    technologies: ['TypeScript', 'ReactJS', 'Node', 'Tailwind'],
    image: '/projects/jasmine.png',
    url: 'https://app.jasmine.energy/',
  },
  {
    title: 'aByte Inc.',
    role: 'Lead Front-End Engineer',
    description: 'aByte is a design, software and branding agency in Vancouver Canada.',
    achievements: [
      'Managed various <b>teams of front-end and back-end engineers (3-7)</b> to deliver projects on time',
      'Successfully launched <b>over 10+ projects</b> within 2 years',
      '• Contributed to projects in various industries such as FinTech, Health, Delivery Systems, Metaverse, Social Media platforms, and more',
    ],
    technologies: ['Jekyll', 'Sass', 'Pug', 'ThreeJS'],
    image: '/projects/abyte.png',
    url: 'https://abyte.ca',
  },
  {
    title: 'DreamWorldNFTs',
    role: 'Lead UI/UX & Full-Stack Engineer',
    description: 'A platform that allows users to mint and trade NFTs.',
    achievements: [
      'Developed the platform from scratch within 2 months',
      'Platform generated <b>revenue of $0.5M within the first 4 hours</b> of launch',
      'Web app processed <b>over 6,000 transactions</b> within 1 hour of launch without any issues',
    ],
    technologies: ['TypeScript', 'ReactJS', 'Next', 'Supabase', 'Tailwind', 'GSAP'],
    image: '/projects/dreamworld.png',
    url: 'https://www.dreamworldnfts.com/',
  },
  {
    title: 'NFTLX',
    role: 'Lead UI/UX & Front-End Engineer',
    description: 'No longer active. NFTLX was one of the first platforms to ',
    achievements: [
      'Developed the platform from scratch within 2 months',
      'Utilized cutting-edge technologies such as <b>Bloto & Flow (blockchain)</b> without much documentation',
      'Platform generated <b>over $100K in revenue</b> within the first 24 hours of launch',
    ],
    technologies: ['TypeScript', 'ReactJS', 'Next', 'Prisma', 'Tailwind', 'Sass', 'ThreeJS'],
    image: '/projects/nftlx.png',
    url: 'https://web.archive.org/web/20220903160932/https://www.nftlx.io/',
  },
  {
    title: 'Candle.gg',
    role: 'Lead UI/UX & Front-End Engineer',
    description: 'Candle.gg is a platform that brings together content creators and influencers to collaborate.',
    achievements: [
      'Completed the initial MVP within 1.5 months from scratch',
      'The platform gained 1,500+ users within the first 2 weeks of launch',
      'Improved overall API response time by 90% by switching stack from MongoDB (Parse) to PostgreSQL (Prisma) and optimizing DB queries.',
    ],
    technologies: ['TypeScript', 'ReactJS', 'Next', 'MUI', 'Prisma', 'Sass'],
    image: '/projects/candlegg.png',
    url: 'https://candle.gg',
  },
];
