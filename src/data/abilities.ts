import { InlineIconDef } from '../components/techchip';

// what I do well: typescript, nodejs, react, tailwind, gsap, svelte, sveltekit, mysql, postgresql, nextjs, html, css, sass, vite
export const whatIDoWellIcons: InlineIconDef[] = [
  {
    name: 'TypeScript',
    fileName: 'typescript',
  },
  {
    name: 'Node.js',
    fileName: 'node',
  },
  {
    name: 'React & React Native',
    fileName: 'reactjs',
  },
  {
    name: 'NextJS',
    fileName: 'next',
  },
  {
    name: 'Tailwind',
    fileName: 'tailwind',
  },
  {
    name: 'GSAP',
    fileName: 'gsap',
  },
  {
    name: 'Svelte & SvelteKit',
    fileName: 'svelte',
  },
  {
    name: 'Docker',
    fileName: 'docker',
  },
  {
    name: 'Github Workflow',
    fileName: 'github',
  },
  {
    name: 'MySQL',
    fileName: 'mysql',
  },
  {
    name: 'PostgreSQL',
    fileName: 'pgsql',
  },
  {
    name: 'HTML',
    fileName: 'html',
  },
  {
    name: 'CSS',
    fileName: 'css',
  },
  {
    name: 'SASS',
    fileName: 'sass',
  },
  {
    name: 'Pug',
    fileName: 'pug',
  },
];

// what I can do: Three.js, python, php
export const whatICanDoIcons: InlineIconDef[] = [
  {
    name: 'Three.js',
    fileName: 'threejs',
  },
  {
    name: 'Framer Motion',
    fileName: 'framermotion',
  },
  {
    name: 'Python',
    fileName: 'python',
  },
  {
    name: 'PHP',
    fileName: 'php',
  },
  {
    name: 'Jekyll',
    fileName: 'jekyll',
  },
];

// what I am learning: Rust, terraform, ansible, aws, vim
export const whatIAmLearningIcons: InlineIconDef[] = [
  {
    name: 'Rust',
    fileName: 'rust',
  },
  {
    name: 'Terraform',
    fileName: 'terraform',
  },
  {
    name: 'Ansible',
    fileName: 'ansible',
  },
  {
    name: 'AWS',
    fileName: 'aws',
  },
  {
    name: 'Vim',
    fileName: 'vim',
  },
];
