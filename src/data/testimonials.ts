import { ITestimonial } from '../components/testimonial';

export const DEFAULT_AVATAR_URL = 'https://avatar.iran.liara.run/public/boy';

export const testimonials: ITestimonial[] = [
  {
    name: 'C Dorn',
    role: 'Chief Technology Officer',
    relation: 'Worked together at GiftCash Inc.',
    content: `I was sad to see Arvin leave our team which he was an integral part of. He is a focused technical project lead who can run with rough ideas and implement clean and well thought out solutions at a rapid pace. He is curious, works well with teams and is self-improving making him an invaluable asset to any team he chooses. I hope to work with Arvin again in the future.`,
  },
  {
    name: 'Danial Khan',
    role: 'Student at SFU',
    avatar: '/testimonials/danial.jpeg',
    relation: 'Reported directly to Arvin',
    content: `I learned a lot from Arvin while working alongside him during my time at AdvisorFlow. This was my first software development position, and I could not have asked for a better mentor and leader. He was always happy to help and was highly approachable when I was looking for guidance. On top of that, his knowledge of full-stack web development is simply outstanding, and he led the team through many complex design problems. Arvin is a very detailed oriented and dedicated developer who will be a great addition to any team. I hope to work with him again in the future.`,
  },
  {
    name: 'Chris Yourth',
    role: 'Intermediate Software Developer',
    avatar: '/testimonials/chris.jpeg',
    relation: 'Reported directly to Arvin',
    content: `Arvin exemplifies a great mentor and leader, not only through his ease in identifying/solving problems, but his ability to give you all the necessary tools to solve it yourself and learn from the experience. Arvin is also a personable guy, someone who you wouldn’t mind hoping on a call with or just chatting about relevant technology. In addition to providing help to the rest of the engineering team, Arvin also found the time to build out complex features and make major refactoring changes. This is where he showed his true expertise and attention to detail when it came to designing and engineering. I have no doubt that any company or organization would be lucky to have Arvin as an employee as he is a true asset to any team he’s on.`,
  },
  {
    name: 'Gautam Ghai',
    role: 'IT Analyst',
    avatar: '/testimonials/gautam.jpeg',
    relation: 'Reported directly to Arvin',
    content: `It was a great experience to work with Arvin. He helped me a lot in learning and improving my skills. He is a great mentor and was always available to answer my doubts and queries. During my time with aByte, Arvin helped me a lot to evolve as a problem solving developer.`,
  },
  {
    name: 'Victor Ramirez',
    role: 'Software Engineer',
    avatar: '/testimonials/victor.jpeg',
    relation: 'Worked directly with Arvin',
    content: `It was great working with Arvin, he is an awesome professional with extense software development knowledge. I liked that he and Yudhvir were very responsible and reliable. They have a great team that delivers quality software, but most importantly they always have a can-do attitude and are passionate for their projects. I hope our paths cross on the future.`,
  },
  {
    name: 'Sahib Singh',
    role: 'Software Developer',
    relation: 'Reported directly to Arvin',
    content: `Arvin is a phenomenal developer who is well-versed in a variety of technologies and design. He’s very calculated, great at conceptualizing, and can trouble-shooting efficiently, making him very dependable for expectations and deadlines. He’s very dedicated at his craft, constantly honing and applying new skills to his work. Arvin is also very cooperative and a pleasure to work with, as he’s willing to go above and beyond for clients and co-workers. Adding Arvin to your team would greatly benefit you!`,
  },
  {
    name: 'Salar Taeb',
    role: 'Devops Developer',
    avatar: '/testimonials/salar.jpeg',
    relation: 'Arvin was senior to Salar',
    content: `I had the pleasure of working with Arvin during my time at AdvisorFlow. Arvin is forward thinking and carefully plans his problem solving approach to deliver the highest standard of work. On multiple occasions Arvin demonstrated his mastery of the React ecosystem, creating solutions that optimized workflows for heavily complex features. Arvin also has a knack for being a team player and effectively collaborating with others to efficiently tackle challenges. Arvin’s work ethic shows his passion for the field, and it is no surprise that he is a master of his craft. Arvin would be a boon to any team or setting he’s placed in.`,
  },
  {
    name: 'Cheery Huang',
    role: 'Talent Manager',
    avatar: '/testimonials/cheery.jpeg',
    relation: 'Arvin’s client',
    content: `Arvin has an incredibly creative eye when it comes to details and design for web development. I had worked with Arvin on Candle.GG where he helped created robust web and mobile pages for the project, and accelerated our growth with his knowledge of SEO development. He's a reliable and hard-working web developer with excellent communication and knowledge. It was a pleasant experience working together with Arvin.`,
  },
  {
    name: 'Yudhvir Raj',
    role: 'Software Engineering Manager',
    avatar: '/testimonials/yudhvir.jpeg',
    relation: 'Managed Arvin directly',
    content: `I have worked with Arvin on numerous projects & in every single instance, I have not had to worry about whether it will get done. Adding to this, he constantly strives to improve & you can see his work benefiting from this obsessive need. Always producing quality work & work recommend him to anybody that see his potential.`,
  },
  {
    name: 'Rafael Freire Kloecking',
    role: 'CS Student',
    avatar: '/testimonials/rafael.jpeg',
    relation: 'Studied together',
    content: `Arvin does a great job teaching Web Development. He is very informative and takes time in his explanations so that students can fully understand the concepts. Arvin is also very helpful in answering any questions one may have and has a wealth of knowledge. I learned a great deal from him and recommend Arvin as a teacher.`,
  },
  {
    name: 'Craig Slagel',
    role: 'Founder of RunGo',
    avatar: '/testimonials/craig.jpeg',
    relation: 'Managed Arvin directly',
    content: `Arvin is one of the best web developers I have worked with. He has been able to quickly understand our complex propriety systems and improve then. Early in 2020 Arvin was able to quickly implement new technology to support virtual races and contactless hotel guides to support our customers. I highly recommend Arvin for any project.`,
  },
];
