import React from "react";
import clsx from "clsx";
import { DEFAULT_AVATAR_URL } from "../../data/testimonials";
import { LinkIcon } from "@heroicons/react/16/solid";

export type ITestimonial = {
  name: string;
  role: string;
  relation: string;
  avatar?: string;
  content: string;
}

interface Props extends ITestimonial {
  className?: string;
}

const Testimonial: React.FC<Props> = ({
  className,

  name,
  role,
  relation,
  avatar,
  content,
}) => {
  return (
    <div className={clsx("bg-slate-50 p-6 rounded-xl @container group hover:bg-slate-100 h-fit", className)}>
      <div className="flex flex-col gap-4 justify-between">
        <div>
          <p className="relative text-slate-600 line-clamp-4">
            {content}
          </p>
          <a
            href="https://www.linkedin.com/in/arvin-ansari/details/recommendations/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Read more <LinkIcon className="h-4 w-4 inline" />
          </a>
        </div>
        <div className="flex flex-row gap-4">
          <img
            className="block w-14 h-14 rounded-full"
            src={avatar || DEFAULT_AVATAR_URL}
            alt={name}
          />
          <div className="flex flex-col gap-4">
            <div>
              <h4 className="text-slate-900 font-semibold">{name}</h4>
              <h5 className="text-blue-400 text-sm font-bold">{role}</h5>
              <p className="text-slate-600">{relation}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Testimonial;