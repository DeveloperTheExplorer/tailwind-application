import React from 'react';
import { motion, MotionProps, Variants } from 'framer-motion';

export interface PopProps extends MotionProps {
  className?: string;
  delay?: number;
  amount?: number;
}

const Pop: React.FC<PopProps> = ({ children, amount, delay, ...rest }) => {
  const popVariant: Variants = {
    offscreen: {
      scale: 0.8,
      opacity: 0,
    },
    onscreen: {
      scale: 1,
      opacity: 1,
      transition: {
        delay: delay || 0,
        type: 'spring',
        bounce: 0.3,
        duration: 0.3,
      },
    },
  };

  return (
    <motion.div
      initial='offscreen'
      whileInView='onscreen'
      viewport={{ once: true, amount: amount || 0.8 }}
      variants={popVariant}
      {...rest}
    >
      {children}
    </motion.div>
  );
};

export default Pop;
