import React from 'react';
import { motion, MotionProps, Variants } from 'framer-motion';

export interface FadeInProps extends MotionProps {
  className?: string;
  delay?: number;
  amount?: number;
}

const FadeIn: React.FC<FadeInProps> = ({ children, delay, amount, ...rest }) => {
  const fadeVariant: Variants = {
    offscreen: {
      y: 25,
      opacity: 0,
    },
    onscreen: {
      y: 0,
      opacity: 1,
      transition: {
        delay: delay || 0,
        type: 'easeOut',
        duration: 0.4,
      },
    },
  };

  return (
    <motion.div
      initial='offscreen'
      whileInView='onscreen'
      viewport={{ once: true, amount: amount || 0.8 }}
      variants={fadeVariant}
      {...rest}
    >
      {children}
    </motion.div>
  );
};

export default FadeIn;
