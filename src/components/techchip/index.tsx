import clsx from 'clsx';
import React from 'react';

export type InlineIconDef = {
  name: string;
  fileName: string;
};

interface TechChipProps extends InlineIconDef {
  className?: string;
}

const TechChip: React.FC<TechChipProps> = ({ name, fileName, className }) => {
  return (
    <div key={fileName} className={clsx('flex flex-row gap-2 rounded-md bg-stone-100 px-2 py-1', className)}>
      <img className='h-6 w-6' alt={`${name} icon`} src={`/code_icons/file_type_${fileName}.svg`} />
      <p className='text-stone-900'>{name}</p>
    </div>
  );
};

export default TechChip;
