import React, { useState } from 'react';
import { Variants, motion } from 'framer-motion';
import clsx from 'clsx';

import FadeIn from '../animated/fadeIn';

interface Props {
  onTerminalSubmission: (input: string) => void;

  showCoverLetter: boolean;
}

const waveVariant: Variants = {
  onscreen: {
    rotate: [0, 10, -10, 10, -10, 0],
    transition: {
      delay: 0.6,
      type: 'easeInOut',
      duration: 1,
    },
  },
};

const LandingHeader: React.FC<Props> = ({ onTerminalSubmission, showCoverLetter }) => {
  const [terminalInput, setTerminalInput] = useState<string>(showCoverLetter ? 'yes' : '');
  const [temrinalCursor, setTerminalCursor] = useState<number>(0);

  const handleTerminalInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTerminalInput(e.target.value);
  };

  const handleTerminalKeyUp = (e: React.KeyboardEvent<HTMLInputElement>) => {
    // detect enter key
    if (e.key === 'Enter') {
      e.preventDefault();
      onTerminalSubmission(terminalInput);
      return;
    }
    setTerminalCursor(e.currentTarget.selectionStart || 0);
  };

  return (
    <header className='pattern relative mb-20 w-full py-40 pt-20 md:pt-40'>
      <img
        className='absolute -top-8 left-[30%] w-[65%] animate-bg-play'
        src='/gradient-blurs.svg'
        alt='background gradient blur'
      />

      <div className='relative mx-auto mt-32 flex h-full min-h-96 max-w-[1300px] flex-col gap-4 px-6 md:mt-64 lg:grid lg:grid-cols-12 lg:px-0'>
        <FadeIn className='col-span-1 md:px-12 lg:px-0'>
          <motion.h1
            whileInView='onscreen'
            viewport={{ once: true, amount: 0.8 }}
            variants={waveVariant}
            className='mt-3 w-fit origin-bottom-right text-7xl'
          >
            👋
          </motion.h1>
        </FadeIn>
        <div className='relative w-full space-y-8 md:w-2/3 md:px-12 lg:col-span-6 lg:w-full lg:px-0 xl:col-span-5 '>
          <FadeIn delay={0.2}>
            <h1 className='text-balance text-5xl'>Welcome! I have been expecting you...</h1>
          </FadeIn>
          <FadeIn delay={0.4}>
            <p className='text-balance'>
              This application is for the Design Engineer position at on of the companies to which I owe much gratitude
              and respect.
            </p>
          </FadeIn>

          <FadeIn delay={0.5} className='flex flex-row items-center justify-between gap-2'>
            <p>That company is of course:</p>
            <img className='h-6 md:h-8' src='/tailwindcss-logotype.svg' alt='TailwindCSS Logo Type' />
          </FadeIn>
        </div>

        <div className='relative col-span-5 col-start-8 -mb-12 md:mt-20 md:px-12 lg:mb-0 lg:px-0'>
          <label
            htmlFor='terminal-input'
            className={clsx(
              'group relative left-0 top-40 z-30 block w-full rounded-xl bg-slate-800 font-mono text-slate-600 shadow-gray-800 transition-colors duration-75 focus-within:animate-none hover:animate-none has-[:focus]:text-slate-400 has-[:focus]:shadow-2xl lg:absolute',
              !showCoverLetter && 'animate-hover',
            )}
          >
            <div className='relative flex w-full flex-row items-center justify-between border-b border-b-slate-500/30 p-2'>
              <div className='relative flex flex-row items-center gap-2 p-2'>
                <span className='h-3 w-3 rounded-full bg-slate-400 group-has-[:focus]:bg-red-400' />
                <span className='h-3 w-3 rounded-full bg-slate-400 group-has-[:focus]:bg-amber-400' />
                <span className='h-3 w-3 rounded-full bg-slate-400 group-has-[:focus]:bg-green-400' />
                <div className='absolute left-16 ml-2 hidden flex-row items-center gap-0.5 leading-none md:flex'>
                  <span className='text-2xl'>⌥⌘</span>2
                </div>
              </div>

              <p className='flex px-4 font-medium tracking-wide'>application@tailwind:~</p>
              <span />
            </div>
            <div className='flex flex-col p-6 text-sm text-slate-300'>
              <p>
                <span className='text-sky-400'>~</span>
                <span className='text-blue-500'>/Desktop/tailwind/application</span>
                <span className='text-green-500'>{' ❯ '}</span>

                <span>npm run arvin:cover_letter</span>
              </p>
              <br />
              <p>Last login: Wed Nov 1 2017</p>
              <br />
              <p>
                [<span className='uppercase text-amber-300'>Warning</span>]: You are about to view a
                <span className='bg-gradient-to-r from-pink-300 to-indigo-400 bg-clip-text font-semibold uppercase text-transparent'>
                  {' Super Dope '}
                </span>
                cover letter. 😎
              </p>
              <br />
              <p className='text-amber-300'>Viewer discretion is advised.</p>
              <p className='italic text-slate-400'>Okay, enough cringe. Please continue reading lol.</p>
              <br />
              <p className='font-bold'>Would you like to continue reading?</p>
              <p className='ml-4'>
                To{' '}
                <span className='inline-block bg-gradient-to-r from-pink-300 to-indigo-400 bg-clip-text font-semibold text-transparent'>
                  continue reading
                </span>
                , type "<span className='text-lime-500'>yes</span>" or "<span className='text-lime-500'>y</span>"
              </p>
              <div className='relative flex flex-row items-center'>
                <input
                  id='terminal-input'
                  type='text'
                  maxLength={30}
                  className='w-full bg-transparent text-[14px] text-slate-300 caret-transparent ring-0 focus-within:outline-none'
                  onKeyUp={handleTerminalKeyUp}
                  onChange={handleTerminalInput}
                  value={terminalInput}
                  disabled={showCoverLetter}
                  onSubmit={e => console.log('sumit', e)}
                />
                <span
                  className='absolute left-0 top-0 hidden h-5 w-2 animate-pulse bg-slate-100 opacity-75 group-has-[:focus]:block'
                  style={{
                    transform: `translateX(${Math.min(temrinalCursor, terminalInput.length) * 8.5}px)`,
                  }}
                />
              </div>
            </div>
          </label>
        </div>
      </div>
    </header>
  );
};

export default LandingHeader;
