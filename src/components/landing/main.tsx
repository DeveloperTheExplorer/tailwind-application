import React from 'react';
import clsx from 'clsx';
import { ClockIcon, CodeBracketSquareIcon, MapPinIcon, StarIcon } from '@heroicons/react/16/solid';

import Safari from '../ios/safari';
import Project from '../project';
import { whatIAmLearningIcons, whatICanDoIcons, whatIDoWellIcons } from '../../data/abilities';
import { projects } from '../../data/projects';
import { testimonials } from '../../data/testimonials';
import Testimonial from '../testimonial';
import TechChip from '../techchip';
import Pop from '../animated/pop';
import FadeIn from '../animated/fadeIn';
import { socials } from '../../data/socials';

interface Props {
  showCoverLetter: boolean;
}

const LandingMain: React.FC<Props> = ({ showCoverLetter }) => {
  const isMobile = window.innerWidth < 640;

  return (
    <main className={clsx('relative z-20 bg-white pt-20 md:pt-40', !showCoverLetter && 'hidden')}>
      <div className='relative mx-auto grid max-w-[1300px] grid-cols-12 gap-8'>
        <div className='col-span-12 px-6 sm:col-span-10 sm:col-start-2 sm:px-0 md:col-span-8 md:col-start-3 xl:col-span-5'>
          <Safari className='@container xl:sticky xl:top-10'>
            <div className='flex flex-row gap-4'>
              <Pop>
                <img
                  className='h-20 w-20 min-w-20 rounded-full'
                  src='/arvin-profile.png'
                  alt="Arvin's profile picture"
                />
              </Pop>
              <div className='flex grow flex-col gap-4'>
                <Pop
                  className='flex flex-col flex-wrap justify-between gap-2 @lg:flex-row @lg:items-center'
                  delay={0.2}
                >
                  <h1 className='text-2xl font-semibold text-slate-900 @md:text-4xl'>Arvin Ansari</h1>
                  <div className='flex flex-row items-center gap-6'>
                    {socials.map((social, i) => (
                      <Pop key={social.url} delay={i * 0.1}>
                        <a href={social.url} target='_blank' rel='noopener noreferrer'>
                          <img className='h-6 w-6' src={`social_icons/${social.icon}`} alt={social.name} />
                        </a>
                      </Pop>
                    ))}
                  </div>
                </Pop>
                <div className='hidden flex-row flex-wrap items-center gap-2 text-slate-400 @lg:flex'>
                  <FadeIn delay={0.3} className='flex flex-row items-center gap-1'>
                    <CodeBracketSquareIcon className='h-6 w-6' />
                    <p className='whitespace-nowrap'>Sr. Full-Stack Engineer</p>
                  </FadeIn>
                  <FadeIn delay={0.4} className='flex flex-row items-center gap-1'>
                    <StarIcon className='h-6 w-6' />
                    <p className='whitespace-nowrap'>Front-End Focused</p>
                  </FadeIn>
                </div>
              </div>
            </div>
            <div className='flex flex-row flex-wrap items-center gap-2 text-slate-400 @lg:hidden'>
              <FadeIn delay={0.3} className='flex flex-row items-center gap-1'>
                <CodeBracketSquareIcon className='h-6 w-6' />
                <p className='whitespace-nowrap'>Sr. Full-Stack Engineer</p>
              </FadeIn>
              <FadeIn delay={0.4} className='flex flex-row items-center gap-1'>
                <StarIcon className='h-6 w-6' />
                <p className='whitespace-nowrap'>Front-End Focused</p>
              </FadeIn>
            </div>
            <FadeIn delay={0.5}>
              <p>
                I picked up web development when I turned 14 (in 2014), and have not stopped since. It's been a journey
                that I wish to continue for as long as my brain and fingers work.
              </p>
            </FadeIn>

            <div className='flex flex-col gap-2'>
              <h5 className='font-bold text-blue-400'>I&apos;m Based in</h5>
              <div className='flex flex-row flex-wrap items-center gap-4 text-slate-400'>
                <FadeIn className='flex flex-row items-center gap-2'>
                  <MapPinIcon className='h-6 w-6' />
                  <p className='whitespace-nowrap'>Vancouver, Canada 🇨🇦</p>
                </FadeIn>
                <FadeIn delay={0.3} className='flex flex-row items-center gap-2'>
                  <ClockIcon className='h-6 w-6' />
                  <p className='whitespace-nowrap font-bold text-black underline'>Can work EST ✅</p>
                </FadeIn>
              </div>
            </div>

            <div className='flex flex-col gap-2'>
              <h5 className='font-bold text-blue-400'>What I do well 😎</h5>
              <div className='flex flex-row flex-wrap items-center gap-2'>
                {whatIDoWellIcons.map((icon, i) => (
                  <Pop delay={i * 0.1} key={icon.fileName}>
                    <TechChip {...icon} />
                  </Pop>
                ))}
              </div>
            </div>

            <div className='flex flex-col gap-2'>
              <h5 className='font-bold text-blue-400'>What I can do 🙂</h5>
              <div className='flex flex-row flex-wrap items-center gap-2'>
                {whatICanDoIcons.map((icon, i) => (
                  <Pop delay={i * 0.1} key={icon.fileName}>
                    <TechChip {...icon} />
                  </Pop>
                ))}
              </div>
            </div>

            <div className='flex flex-col gap-2'>
              <h5 className='font-bold text-blue-400'>What I am learning 📖</h5>
              <div className='flex flex-row flex-wrap items-center gap-2'>
                {whatIAmLearningIcons.map((icon, i) => (
                  <Pop delay={i * 0.1} key={icon.fileName}>
                    <TechChip {...icon} />
                  </Pop>
                ))}
              </div>
            </div>
          </Safari>
        </div>

        <div className='col-span-12 px-6 md:col-span-10 md:col-start-2 md:px-0 xl:col-span-7'>
          <h5 className='font-bold text-blue-400'>Projects</h5>
          <h2>Here are some of the projects I was involved in:</h2>

          <div className='mt-8 grid grid-cols-6 gap-4'>
            {projects.map((project, i) => {
              const delay = isMobile ? 0 : ((i * 3) % testimonials.length) * 0.05;
              const amount = isMobile ? 0.5 : 0.2;

              return (
                <FadeIn
                  key={project.title}
                  delay={delay}
                  amount={amount}
                  className='col-span-6 first:col-span-6 lg:col-span-3'
                >
                  <Project {...project} />
                </FadeIn>
              );
            })}
          </div>

          <h5 className='mt-12 font-bold text-blue-400'>Excitement</h5>
          <h2>What I am excited about</h2>
          <article className='prose'>
            <ul>
              <li>
                Crafting the Catalyst components. I believe Tailwind Catalyst can be even greater than Shadcn UI. I love
                Shadcn UI and I think it's a great library but I believe we can do better. First off, we will be
                building our components from scratch. This will allow us to have a better understanding of the
                components and how they work. We can also forward more features and make the components more
                customizable.
              </li>
              <li>
                Taking part in the TailwindCSS redesign. I already love the existing TailwindCSS website. This cover
                letter I developed was <u>heavily inspired by you guys</u>. I can't imagine what you guys have in store
                for the redesign. I believe it will be challenging but I would love to be a part of it.
              </li>
              <li>
                Writing high-quality code that is easy to maintain and yet used by millions of developers. I believe
                this is every software engineer's dream. I believe I can achieve this dream at TailwindCSS.
              </li>
              <li>
                Learning Rust and being able to contribute Rust code to a meaningful project. I am just picking it up
                and I am loving it. Currently on{' '}
                <a
                  href='https://doc.rust-lang.org/book/ch08-00-common-collections.html'
                  target='_blank'
                  rel='noopener noreferrer'
                >
                  Section 8, Common Collections.
                </a>
              </li>
            </ul>
          </article>

          <h5 className='mt-12 font-bold text-blue-400'>Open-Source</h5>
          <h2>Some Pull requests and issues</h2>
          <article className='prose mt-2'>
            <p>
              I admit that when it comes to open-source, I have not contributed as much as I would have liked. Without
              sounding like I am making excuses, I simply had not had the chance to get involved. However, since last
              year, I have been making a conscious effort to contribute to open-source projects.
            </p>
            <ul>
              <li>
                <b>Inquirer.js</b>
                <a className='ml-2' href='https://github.com/SBoudrias/Inquirer.js/pull/1110'>
                  PR: #1110 ESM Migration
                </a>
                <ul>
                  <li>
                    Worked together with my colleague to push this out. Prior to this PR, you could not use inquirer in
                    an ESM project.
                  </li>
                  <li>
                    Rewrote <b>1,000+ lines and many tests</b> to migrate the code.
                  </li>
                  <li>
                    Unfortunately, because I contributed to my colleague's fork, I am not listed as a contributor to the
                    library, however, you can view my commits in the Pull Request.
                  </li>
                </ul>
              </li>
              <li>
                <b>HeadlessUI</b>
                <a className='ml-2' href='https://github.com/tailwindlabs/headlessui/pull/2944'>
                  PR: #2944 Added Events to Dialogue onClose
                </a>
                <ul>
                  <li>
                    Currently, it is not possible to discriminate based on onClose events for the Dialogue component.
                  </li>
                  <li>
                    While wrapping the <code>{'<ToastContainer .../> with <Portal></Portal>'}</code>{' '}
                    <a
                      href='https://github.com/tailwindlabs/headlessui/pull/1268#issuecomment-1082001717'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      (as suggested here)
                    </a>{' '}
                    tags allows the toasts to show up on top as expected, anytime they are clicked, the dialog is
                    dismissed, which can lead to unexpected behaviour.
                  </li>
                  <li>
                    While I tried to limit my modifications as much as possible,{' '}
                    <a
                      href='https://github.com/tailwindlabs/headlessui/pull/2944#issuecomment-1992758616'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      I offered to reformat the code to the spec that TailwindCSS would like me to
                    </a>
                    , however, I have not heard anything back since.
                  </li>
                </ul>
              </li>
              <li>
                <b>react-native-reanimated</b>
                <a className='ml-2' href='https://github.com/software-mansion/react-native-reanimated/issues/3796'>
                  Issue: #3796 Configuration Error
                </a>
                <ul>
                  <li>
                    Reported the issue as best I could, however, realized the issue was not due to the library, but
                    rather in the configuration
                  </li>
                  <li>
                    While wrapping the <code>{'<ToastContainer .../> with <Portal></Portal>'}</code>{' '}
                    <a
                      href='https://github.com/tailwindlabs/headlessui/pull/1268#issuecomment-1082001717'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      (as suggested here)
                    </a>{' '}
                    tags allows the toasts to show up on top as expected, anytime they are clicked, the dialog is
                    dismissed, which can lead to unexpected behaviour.
                  </li>
                  <li>
                    To my surprise, <b>many others also experienced this issue</b> and found the issue and its
                    resolution{' '}
                    <a
                      href='https://github.com/software-mansion/react-native-reanimated/issues/3796#issuecomment-1325782665'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      quite helpful
                    </a>
                  </li>
                  <li>
                    One person even <b>offered to kiss me</b>:{' '}
                    <a
                      href='https://github.com/software-mansion/react-native-reanimated/issues/3796#issuecomment-1404017548'
                      target='_blank'
                      rel='noopener noreferrer'
                    >
                      click here for proof
                    </a>
                    .
                  </li>
                </ul>
              </li>
              <li>
                <b>wagmi</b>
                <a className='ml-2' href='https://github.com/wevm/wagmi/issues/2909'>
                  Issue: #2909 TypeError
                </a>
                <ul>
                  <li>
                    Although the issue was not a bug, it was undocumented. I tried reporting the bug as best as I could.
                  </li>
                  <li>
                    Later figured out the problem and reported the solution in the issue in the same thread, which some
                    found quite useful.
                  </li>
                </ul>
              </li>
            </ul>
          </article>

          <h5 className='mt-12 font-bold text-blue-400'>Teaching</h5>
          <h2>My Teaching Background</h2>
          <article className='prose'>
            <ul>
              <li>
                <b>Mentoring & Teaching</b>
                <a className='ml-2' href='https://www.lighthouselabs.ca/'>
                  Lighthouse Labs
                </a>
                <ul>
                  <li>
                    Worked as a mentor and later as an instructor in one of <b>Canada's leading bootcamps</b>.
                  </li>
                  <li>
                    Received an overall rating of over <b>4.75/5 rating</b> as a mentor.
                  </li>
                  <li>
                    After 6-7 months working as a mentor, I was <b>offered to teach my own class</b> for existing IT and
                    software engineers who wished to part take in an upskilling on a course about full-stack web
                    development.
                  </li>
                  <li>Received an overall 80% approval rating on my performance as an instructor.</li>
                </ul>
              </li>
              <li>
                <b>{'Guest Lecturer: '}</b>
                <a href='https://www.vcc.ca/'>Vancouver Community College</a>
                <ul>
                  <li>Was invited to teach a guest lecture on front-end development and front-end fundamentals.</li>
                  <li>Covered how to effectively write Sass (&CSS) code to build UI quickly.</li>
                  <li>Was later re-invited to help evaluate student project submissions.</li>
                </ul>
              </li>
              <li>
                <b>Mentoring:</b> Helping out friends, colleagues, and classmates with their coding problems at SFU.
                <ul>
                  <li>
                    In addition to the experiences above, I have always helped out anyone I could with any issues within
                    my domain.
                  </li>
                  <li>Keep scrolling for a list of testimonials, some of which are about my role as a mentor.</li>
                </ul>
              </li>
            </ul>
          </article>

          <h5 className='mt-12 font-bold text-blue-400'>Final Notes</h5>
          <h2>Why I value TailwindCSS</h2>
          <article className='prose mt-2'>
            <p>
              For as long as I can remember, I have loved working with CSS. Part of the reason I continued to develop
              websites was because of the joy I got from making things look good.
              <br />
              <br />
              However, the joy was always short-lived. At first, everything felt magical but as the project scope grew,
              the CSS files became more and more complex and harder to maintain. A small change in one place would break
              something in another place.
              <br />
              <br />
              Then the libraries came. Bootstrap, Foundation, Materialize, Semantic UI, and many more. They all helped
              but they were not the solution. My biggest issues were always with the custom styling. I wanted to make
              things look unique, and the libraries were not helping.
              <br />
              <br />
              For the most part, Sass helped in a lot of ways. It sped up development significantly but it had the same
              issues with vanilla CSS. It was still CSS. As complexity grew, the Sass files became harder to maintain.
              <br />
              <br />
              I remember back in 2020, my colleague introduced me to TailwindCSS. I was skeptical at first. I thought
              the approach was rather backwards. Even though, I had built a similar library for my use (at a much
              smaller scale). I gave it a try and over time I realized how much quicker I had become at building UIs.
              And of course, with the added benefit of writing maintainable code.
              <br />
              <br />I value TailwindCSS' vision and I believe I can contribute to it. I believe that TailwindCSS has the
              potential to change the way websites are made fundamentally at the browser level. I would love to be a
              part of this change.
            </p>
          </article>
        </div>

        <div className='col-span-12 mt-12 px-6 lg:col-span-10 lg:col-start-2 xl:px-0'>
          <div className='text-center'>
            <h5 className='font-bold text-blue-400'>Testimonials</h5>
            <h2>Here are a few reasons to hire me:</h2>
          </div>

          <div className='mt-8 grid w-full grid-cols-12 gap-4'>
            {testimonials.map((testimonial, i) => {
              const delay = isMobile ? 0 : ((i * 3) % testimonials.length) * 0.05;
              const amount = isMobile ? 0.5 : 0.2;

              return (
                <Pop
                  key={testimonial.name}
                  delay={delay}
                  amount={amount}
                  className='col-span-12 md:col-span-6 md:first:col-span-6 md:first:col-start-4 md:last:col-span-6 xl:col-span-4 xl:last:col-start-4'
                >
                  <Testimonial {...testimonial} />
                </Pop>
              );
            })}
          </div>
        </div>
      </div>
      <footer className='w-full p-12 text-center'>
        <p className='text-balance'>
          Thank you so much for your time and consideration.
          <br />I look forward to hearing from you soon.
          <br />
          <a
            className='mx-auto mt-4 flex w-fit items-center gap-2 rounded-md bg-slate-900 px-4 py-2 text-white no-underline hover:bg-slate-800'
            href='https://gitlab.com/DeveloperTheExplorer/tailwind-application'
            target='_blank'
            rel='noopener noreferrer'
          >
            View This on <img src='/code_icons/file_type_gitlab.svg' alt='GitLab Icon' className='h-6 w-auto' />
          </a>
        </p>
      </footer>
    </main>
  );
};

export default LandingMain;
