import {
  ArrowPathIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  LockClosedIcon,
  PlusIcon,
  ShareIcon,
} from '@heroicons/react/16/solid';
import clsx from 'clsx';
import React from 'react';
import FadeIn from '../animated/fadeIn';

interface Props {
  className?: string;
  children: React.ReactNode;
}

const Safari: React.FC<Props> = ({ className, children }) => {
  return (
    <FadeIn
      amount={0.3}
      className={clsx('w-full rounded-xl bg-white shadow-xl ring ring-slate-900/5 @container', className)}
    >
      <div className='flex flex-row items-center justify-between bg-gradient-to-b from-neutral-50 to-stone-100 p-2'>
        <div className='flex flex-row items-center gap-2 pl-2'>
          <span className='h-3 w-3 rounded-full bg-red-400' />
          <span className='h-3 w-3 rounded-full bg-amber-400' />
          <span className='h-3 w-3 rounded-full bg-green-400' />
          <ChevronLeftIcon className='ml-4 hidden h-5 w-5 text-slate-400 @lg:block' />
          <ChevronRightIcon className='hidden h-5 w-5 text-slate-400 @lg:block' />
        </div>
        <div className='mr-4 flex w-1/2 items-center justify-between rounded-md border border-slate-900/5 bg-slate-100 px-2 py-1 text-xs font-medium'>
          <div />
          <div className='flex flex-row items-center gap-2'>
            <LockClosedIcon className='mr-1 h-4 w-4 text-slate-400' />
            about.me
          </div>
          <ArrowPathIcon className='ml-1 h-4 w-4 text-slate-400' />
        </div>
        <div className='flex flex-row items-center gap-3'>
          <ShareIcon className='mr-1 hidden h-4 w-4 text-slate-400 @lg:block' />
          <PlusIcon className='mr-1 hidden h-4 w-4 text-slate-400 @lg:block' />
        </div>
      </div>
      <div className='relative flex flex-col gap-8 rounded-b-xl border-t border-slate-200 bg-white p-6'>{children}</div>
    </FadeIn>
  );
};

export default Safari;
