import React from 'react';
import clsx from 'clsx';
import TechChip from '../techchip';

export type IProject = {
  title: string;
  role: string;
  description: string;
  technologies: string[];
  achievements: string[];
  image: string;
  url: string;
};

interface Props extends IProject {
  className?: string;
}

const Project: React.FC<Props> = ({
  className,

  title,
  role,
  description,
  technologies,
  achievements,
  image,
  url,
}) => {
  return (
    <div className={clsx('group rounded-xl bg-slate-50 p-6 @container hover:bg-slate-100', className)}>
      <div className='flex flex-col-reverse justify-between gap-4 @lg:flex-row'>
        <div className='flex flex-col gap-4 @lg:w-1/2'>
          <div>
            <h2 className='text-2xl font-semibold text-slate-900'>{title}</h2>
            <h4 className='text-sm font-bold text-blue-400'>{role}</h4>
          </div>
          <p className='text-slate-600'>{description}</p>
          {/* achievements: */}
          <ul className='flex flex-col gap-2'>
            {achievements.map(achievement => (
              <li key={achievement} className='flex flex-row gap-2'>
                <span className='text-lg text-blue-400'>🏆</span>
                <p className='text-slate-600' dangerouslySetInnerHTML={{ __html: achievement }}></p>
              </li>
            ))}
          </ul>
          <div className='flex flex-row flex-wrap gap-2 @lg:hidden'>
            {technologies.map(icon => {
              const fileName = icon.replace(/\s/g, '_').toLowerCase();
              return <TechChip key={`${title}-${fileName}`} name={icon} fileName={fileName} />;
            })}
          </div>

          <a
            href={url}
            target='_blank'
            rel='noreferrer'
            className='self-start rounded-lg bg-blue-400 px-4 py-2 font-medium text-white no-underline'
          >
            View Project
          </a>
        </div>
        <div className='flex w-full flex-col gap-2 @lg:w-1/2'>
          <div className='flex h-auto flex-col overflow-hidden rounded-xl'>
            <img
              src={image}
              alt={title}
              className='aspect-square h-auto w-full object-cover transition-transform duration-300 ease-out group-hover:scale-125'
            />
          </div>
          <div className='hidden flex-row flex-wrap gap-2 @lg:flex'>
            {technologies.map(icon => {
              const fileName = icon.replace(/\s/g, '_').toLowerCase();
              return <TechChip key={`${title}-${fileName}`} name={icon} fileName={fileName} />;
            })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Project;
