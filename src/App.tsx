import React, { useState } from 'react';

import LandingHeader from './components/landing/header';
import LandingMain from './components/landing/main';

const keepReadingMessages = [
  'Yeaaah... so... this is kinda awkward. But I kinda need you to type "yes" or "y" to continue reading. 😅',
  'Try typing "yes" or "y" to continue reading. 🙂',
  'Almost... but not quite. Try typing "yes" or "y" to continue reading. 🤏',
  'My fellow reader, I need you to type "yes" or "y" to continue reading. 🙏',
  'I don\'t mean to be rude, but I kinda need you to type "yes" or "y" to continue reading. 😅',
  'Try this: click on the terminal and type "yes" or "y" to continue reading. 👀',
  'I need you to type "yes" or "y" to continue reading. 🙄',
  'We can do this the easy way or the hard way. Type "yes" or "y" to continue reading. 🙂',
];

function App() {
  const [showCoverLetter, setShowCoverLetter] = useState<boolean>(false);

  const handleTerminalSubmission = (text: string) => {
    if (text.toLowerCase().trim() === 'yes' || text.toLowerCase().trim() === 'y') {
      // show the cover letter
      setShowCoverLetter(true);
      setTimeout(() => window.scrollTo({ top: window.scrollY + window.innerHeight * 0.75, behavior: 'smooth' }), 300);

      return;
    }
    const randomMessage = keepReadingMessages[Math.floor(Math.random() * keepReadingMessages.length)];
    alert(randomMessage);
  };

  return (
    <React.Fragment>
      <LandingHeader onTerminalSubmission={handleTerminalSubmission} showCoverLetter={showCoverLetter} />

      <LandingMain showCoverLetter={showCoverLetter} />
    </React.Fragment>
  );
}

export default App;
