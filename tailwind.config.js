import tailwindTypography from '@tailwindcss/typography';
import containerQueries from '@tailwindcss/container-queries';

/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      keyframes: {
        hover: {
          '0%, 100%': { transform: 'translateY(-0.25rem)' },
          '50%': { transform: 'translateY(0.25rem)' },
        },
        'bg-play': {
          '0%, 100%': { transform: 'translateY(-1rem) rotate(-10deg)' },
          '50%': { transform: 'translateY(1rem) rotate(10deg)' },
        },
      },
      animation: {
        hover: 'hover 1.5s ease-in-out infinite',
        'bg-play': 'bg-play 5s ease-in-out infinite',
      },
    },
  },
  plugins: [tailwindTypography, containerQueries],
};
